#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
    TypeTag type;
    union {
        int value;
        struct {
            Node *left;
            Node *right;
        } function;
    } data;
} Node;

typedef enum TypeTag {
    INT,
    ADD,
    SUB,
    MUL,
    DIV,
    ABS,
    FIBO
} TypeTag;

Node* makeInt(int value) {
    Node *node = (Node*)malloc(sizeof(Node));
    node->type = INT;
    node->data.value = value;
    return node;
}

Node* makeFunc(TypeTag type) {
    Node *node = (Node*)malloc(sizeof(Node));
    node->type = type;
    node->data.function.left = NULL;
    node->data.function.right = NULL;
    return node;
}

void calc(Node *node) {
    if (node == NULL) return;

    if (node->type == INT) {
        // do nothing
    } else if (node->type == ADD) {
        calc(node->data.function.left);
        calc(node->data.function.right);
        node->data.value = node->data.function.left->data.value
                           + node->data.function.right->data.value;
    } else if (node->type == SUB) {
        calc(node->data.function.left);
        calc(node->data.function.right);
        node->data.value = node->data.function.left->data.value
                           - node->data.function.right->data.value;
    } else if (node->type == MUL) {
        calc(node->data.function.left);
        calc(node->data.function.right);
        node->data.value = node->data.function.left->data.value
                           * node->data.function.right->data.value;
    } else if (node->type == DIV) {
        calc(node->data.function.left);
        calc(node->data.function.right);
        node->data.value = node->data.function.left->data.value
                           / node->data.function.right->data.value;
    } else if (node->type == ABS) {
        calc(node->data.function.left);
        node->data.value = abs(node->data.function.left->data.value);
    } else if (node->type == FIBO) {
        calc(node->data.function.left);
        int n = node->data.function.left->data.value;

        int fibo[n+1];
        fibo[0] = 0;
        fibo[1] = 1;
        for (int i = 2; i <= n; i++) {
            fibo[i] = fibo[i-1] + fibo[i-2];
        }

        node->data.value = fibo[n];
    }
}

int main() {
    Node *add = (*makeFunc(ADD))(10, 6);
    Node *mul = (*makeFunc(MUL))(5, 4);
    Node *sub = (*makeFunc(SUB))(add, mul);
    Node *fibo = (*makeFunc(FIBO))(abs(sub), NULL);

    calc(add);
    calc(mul);
    calc(sub);
    calc(fibo);

    printf("add : %d\n", add->data.value);
    printf("mul : %d\n", mul->data.value);
    printf("sub : %d\n", sub->data.value);
    printf("fibo : %d\n", fibo->data.value);

    return 0;
}
